﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EchoChat.Models
{
    public class AppNotice
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public string UserID { get; set; }
        public int Parent { get; set; }

    }
}
