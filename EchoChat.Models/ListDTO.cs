﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EchoChat.Models
{
    public class ListDTO
    {
        public int ListID { get; set; }
        public string Name { get; set; }
        public Guid CreatedBy { get; set; }
        public int ClientID { get; set; }

        public string CreatedByUser { get; set; }
        public int TotalMembers { get; set; }
        public bool? IsPrivate { get; set; }
    }

    public class ListSmallDTO
    {
        public int ListID { get; set; }
        public string Name { get; set; }
    }
}
