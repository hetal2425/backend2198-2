﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EchoChat.Models
{
    public class UserChannelInfo
    {
        public Guid UserID { get; set; }
        public string UserName { get; set; }
        public string EmailID { get; set; }
        public string MobileNo { get; set; }
        public string FacebookID { get; set; }
        public string DeviceToken { get; set; }
        public string ConnectionID { get; set; }
        public string SlackID { get; set; }
    }

    public class UserChannelData
    {
        public string UserID { get; set; }
        public string UserName { get; set; }
        public string DisplayName { get; set; }
        public string EmailID { get; set; }
        public string MobileNo { get; set; }
        public string CustomDetails_1 { get; set; } // WAMobileNo
        public string CustomDetails_2 { get; set; } // DeviceToken
        public string CustomDetails_3 { get; set; } // FacebookID
        public string SlackID { get; set; }
    }

    public class UserChannelDataWithGroup
    {
        public List<UserChannelInfo> UserChannelInfoList { get; set; }
        public string GroupName { get; set; }
    }
}