﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EchoChat.Models
{
   public class UserListDTO
    {
        public int UserListId { get; set; }
        public Guid UserId { get; set; }
        public int ListId { get; set; }
        public bool? IsAdmin { get; set; }
    }
}
