﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EchoChat.Models
{
    public class GroupListMapping
    {
        public int GroupID { get; set; }
        public List<int> ListIDs { get; set; }
    }

    public class GroupAdminMapping
    {
        public int GroupID { get; set; }
        public List<Guid> UserIDs { get; set; }
        public Guid CreatedBy { get; set; }
    }
}
