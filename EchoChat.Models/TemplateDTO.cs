﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EchoChat.Models
{
    public class TemplateDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Body { get; set; }
        public string Type { get; set; }
        public bool IsSms { get; set; }
        public bool IsEmail { get; set; }
        public bool IsWhatsapp { get; set; }
        public bool IsFacebook { get; set; }
        public bool IsSlack { get; set; }
        public bool IsInstagram { get; set; }
        public bool IsViber { get; set; }
        public bool IsLine { get; set; }
        public bool IsWeChat { get; set; }
        public bool IsPushNotification { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class ListOfTemplates
    {
        public int Total { get; set; }
        public List<TemplateDTO> TemplateList { get; set; }
    }
}
