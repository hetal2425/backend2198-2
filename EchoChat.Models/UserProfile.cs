﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EchoChat.Models
{
    public class UserProfile

    {
        public string UserID { get; set; }
        public string FirstName { get; set; }
        public string EmailID { get; set; }
        public string MobileNo { get; set; }
        public IFormFile ProfileImage { get; set; }
        public int ClientID { get; set; }
        public string SlackId { get; set; }
        public string Password { get; set; }

    }
}
