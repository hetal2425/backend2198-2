﻿using System;
using System.Collections.Generic;

namespace EchoChat.Models
{
    public partial class AppSettings
    {
        public int AppSettingId { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        public string Details { get; set; }
        public string Value1 { get; set; }
        public string Value2 { get; set; }
        public string Value3 { get; set; }
        public int? EntityId { get; set; }
        public string EntityName { get; set; }
    }
}
