﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EchoChat.Models
{
    public class NoticeSignalRData
    {
        public string FileName { get; set; }
        public int GroupID { get; set; }
        public string NoticeDetail { get; set; }
        public string NoticeDate { get; set; }
        public int NoticeID { get; set; }
        public string NoticeTitle { get; set; }
        public int? ParentID { get; set; }
        public string FilePath { get; set; }
        public Guid UserID { get; set; }
    }

    public class NoticeSmallData
    {
        public string FileName { get; set; }
        public int IsReply { get; set; }
        public string NoticeDetail { get; set; }
        public string NoticeDetailHTML { get; set; }
        public string NoticeDate { get; set; }
        public string LastReplyDate { get; set; }
        public int NoticeID { get; set; }
        public string NoticeTitle { get; set; }
        public string UserName { get; set; }
        public string LatestMessage { get; set; }
        public Guid UserID { get; set; }
        public string FilePath { get; set; }
        public bool? IsRead { get; set; }
        public string MobileNo { get; set; }
    }

    public class NoticeSmallDataModel
    {
        public int Total { get; set; }
        public List<NoticeSmallData> NoticeSmallDatas { get; set; }
    }

    public class NoticeAnalyzer
    {
        public string Title { get; set; }
        public string Text { get; set; }
    }

    public class HubNotice  
    {
        public int NoticeID { get; set; }
        public string NoticeDetail { get; set; }
        public DateTime NoticeDate { get; set; }
        public int GroupID { get; set; }
        public int ParentID { get; set; }
        public Guid UserID { get; set; }
        public string MobileNo { get; set; }
        public string UserName { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
    }
}
