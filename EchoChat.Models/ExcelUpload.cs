﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EchoChat.Models
{
    public class ExcelUpload
    {
        public int ListID { get; set; }
        public string ListName { get; set; }
        public IFormFile ExcelFile  { get; set; }
    }
    public class ExcelUsers
    {
        public int ListID { get; set; }
        public string ListName { get; set; }
        public List<ExcelUserData> UserList { get; set; }

    }

    public class ExcelUserData
    {
        public string FirstName { get; set; }
        public string Email { get; set; }
        public string MobileNo { get; set; }
        public string IsAdmin { get; set; }
        public bool IsValid { get; set; }
        public string ErrorMessage { get; set; }
    }
}
