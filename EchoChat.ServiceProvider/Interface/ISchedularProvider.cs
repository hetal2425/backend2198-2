﻿using EchoChat.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EchoChat.ServiceProvider
{
    public interface ISchedularProvider
    {
        void AddSchedular(SchedularDTO schedular);
        List<SchedularDTO> GetSchedular();
        void RemoveSchedule(string schedularIds);
        ListOfSchedular GetSchedulePagedData(DateTime? startDate, DateTime? endDate, int pageSize, int pageNo);
        string GenerateJwtTokenForSchedular(LoginRequest user, string timeZoneOffset);
    }
}
