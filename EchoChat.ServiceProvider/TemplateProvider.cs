﻿using EchoChat.Models;
using EchoChat.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace EchoChat.ServiceProvider
{
    public class TemplateProvider : ITemplateProvider
    {
        ITemplateRepository templateRepository;
        public TemplateProvider(ITemplateRepository oTemplateRepository)
        {
            templateRepository = oTemplateRepository;
        }
        public void AddTemplate(TemplateDTO template)
        {
            try
            {
                templateRepository.AddTemplate(template);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool CheckTemplateExist(int id)
        {
            try
            {
                return templateRepository.CheckTemplateExist(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool CheckTemplateExistByName(string name)
        {
            try
            {
                return templateRepository.CheckTemplateExistByName(name);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ListOfTemplates GetTemplate(string title, string type, int pageSize, int pageNo)
        {
            try
            {
               return templateRepository.GetTemplate(title, type, pageSize, pageNo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveTemplate(int id)
        {
            try
            {
                templateRepository.RemoveTemplate(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateTemplate(TemplateDTO template)
        {
            try
            {
                templateRepository.UpdateTemplate(template);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
