﻿
using EchoChat.Models;
using EchoChat.Repository;
using System;
using System.Collections.Generic;

namespace EchoChat.ServiceProvider
{
    public class ClientProvider : IClientProvider
    {
        IClientRepository clientRepository;
        public ClientProvider(IClientRepository oClientRepository)
        {
            clientRepository = oClientRepository;
        }

        public bool CheckClientIsAlreadyRegistered(string email, string mobileNo)
        {
            try
            {
                return clientRepository.CheckClientIsAlreadyRegistered(email, mobileNo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<AppSettingsVM> GetAppSettingsByEntityID(int entityID)
        {
            try
            {
                return clientRepository.GetAppSettingsByEntityID(entityID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetClientEmailById(int Id) {
            try
            {
                return clientRepository.GetClientEmailById(Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public int SaveClient(ClientDTO client)
        {
            try
            {
                return clientRepository.SaveClient(client);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
