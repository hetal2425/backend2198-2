﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using EchoChat.ChannelModels;
using Newtonsoft.Json;

namespace EchoChat.ChannelVerification
{
    public static class WhatsAppVerification
    {
        public static bool VerifyWhatsAppNumber(string waAPI, string password, string mobileNo)
        {
            if (string.IsNullOrEmpty(waAPI) || string.IsNullOrEmpty(password))
            {
                return false;
            }

            string token = GetWhatsAppToken(waAPI, password);

            if (!string.IsNullOrEmpty(token))
            {
                var contact = new
                {
                    contacts = new string[] { mobileNo },
                    force_check = false
                };


                if (!string.IsNullOrEmpty(mobileNo))
                {
                    using (var httpClient = new HttpClient())
                    {
                        httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                        StringContent content = new StringContent(JsonConvert.SerializeObject(contact), Encoding.UTF8, "application/json");

                        using (var response = httpClient.PostAsync(waAPI + "contacts/", content))
                        {
                            response.Wait();
                            var apiResponse = response.Result.Content.ReadAsStringAsync();
                            apiResponse.Wait();
                            ContactResponse waResponse = JsonConvert.DeserializeObject<ContactResponse>(apiResponse.Result);

                            if (waResponse != null)
                            {
                                if (waResponse?.contacts != null && waResponse.contacts.Count > 0)
                                {
                                    if (waResponse?.contacts[0].status == "valid")
                                    {
                                        return true;
                                    }
                                    else if (waResponse?.contacts[0].status == "processing")
                                    {
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    return false;
                }
            }

            return false;
        }

        public static string GetWhatsAppToken(string waAPI, string password)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", password);
                StringContent content = new StringContent(JsonConvert.SerializeObject(""), Encoding.UTF8, "application/json");

                using (var response = httpClient.PostAsync(waAPI + "users/login", content))
                {
                    response.Wait();
                    var apiResponse = response.Result.Content.ReadAsStringAsync();
                    apiResponse.Wait();
                    WAUsers waResponse = JsonConvert.DeserializeObject<WAUsers>(apiResponse.Result);
                    if (waResponse != null)
                    {
                        if (waResponse.users != null && waResponse.users.Count > 0)
                            return waResponse.users[0].token;
                    }
                    return string.Empty;
                }
            }
        }
    }
}