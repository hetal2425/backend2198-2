﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EchoChat.DB
{
    public partial class ClientDeviceMapping
    {
        public int Id { get; set; }
        public int? ClientId { get; set; }
        public string DeviceNo { get; set; }
        public bool? IsHostel { get; set; }
    }
}
