﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EchoChat.DB
{
    public partial class WhatsAppEvent
    {
        public int WhatsAppEventId { get; set; }
        public string RecipientId { get; set; }
        public string MsgId { get; set; }
        public string Status { get; set; }
        public DateTime? Timestamp { get; set; }
    }
}
