﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EchoChat.DB
{
    public partial class GroupUserAdmin
    {
        public int GroupUserAdminId { get; set; }
        public int GroupId { get; set; }
        public Guid UserId { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
