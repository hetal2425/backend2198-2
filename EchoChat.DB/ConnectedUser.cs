﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EchoChat.DB
{
    public partial class ConnectedUser
    {
        public Guid UserId { get; set; }
        public string ConnectionId { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
