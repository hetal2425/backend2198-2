﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EchoChat.DB
{
    public partial class Schedular
    {
        public int Id { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string ReceiverUserId { get; set; }
        public string Message { get; set; }
        public DateTime? ScheduleTime { get; set; }
        public DateTime? Timestamp { get; set; }
    }
}
