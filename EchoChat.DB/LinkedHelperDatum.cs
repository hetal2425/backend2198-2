﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EchoChat.DB
{
    public partial class LinkedHelperDatum
    {
        public int LinkedHelperDataId { get; set; }
        public string MemberId { get; set; }
        public string ProfileUrl { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Industry { get; set; }
        public string Address { get; set; }
        public string Birthday { get; set; }
        public string Headline { get; set; }
        public string CurrentCompany { get; set; }
        public string CurrentCompanyCustom { get; set; }
        public string CurrentCompanyPosition { get; set; }
        public string CurrentCompanyCustomPosition { get; set; }
        public string Organization1 { get; set; }
        public string OrganizationUrl1 { get; set; }
        public string OrganizationTitle1 { get; set; }
        public string OrganizationLocation1 { get; set; }
        public string OrganizationWebsite1 { get; set; }
        public string Organization2 { get; set; }
        public string OrganizationTitle2 { get; set; }
        public string Education1 { get; set; }
        public string EducationDegree1 { get; set; }
        public string Phone1 { get; set; }
        public string PhoneType1 { get; set; }
        public string Phone2 { get; set; }
        public string PhoneType2 { get; set; }
        public string Messenger1 { get; set; }
        public string MessengerProvider1 { get; set; }
        public string Website1 { get; set; }
        public string ThirdPartyEmail1 { get; set; }
        public string CampaignId { get; set; }
        public string CampaignName { get; set; }
    }
}
