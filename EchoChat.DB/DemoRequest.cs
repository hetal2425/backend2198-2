﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EchoChat.DB
{
    public partial class DemoRequest
    {
        public int RequestId { get; set; }
        public string Name { get; set; }
        public string MobileNumber { get; set; }
        public string EmailId { get; set; }
        public DateTime? DateTime { get; set; }
        public string Remark { get; set; }
        public string PageSource { get; set; }
        public string Org { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
    }
}
