﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EchoChat.DB
{
    public partial class Log
    {
        public int LogId { get; set; }
        public string Details { get; set; }
        public string Module { get; set; }
        public string Type { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? ModuleId { get; set; }
    }
}
