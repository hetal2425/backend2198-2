﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EchoChat.DB
{
    public partial class ReceiveWhatsApp
    {
        public int ReceiveWhatsAppId { get; set; }
        public string From { get; set; }
        public string MsgId { get; set; }
        public string Text { get; set; }
        public DateTime? Timestamp { get; set; }
        public string Type { get; set; }
    }
}
