﻿namespace EchoChat.Constants
{
    public class EchoFileType
    {
        public static string Image = "Image";
        public static string Audio = "Audio";
        public static string Video = "Video";
        public static string Document = "Document";
        public static string Button = "Button";
    }
}