﻿namespace EchoChat.Constants
{
    public class LogType
    {
        public static string Add = "Add";
        public static string Update = "Update";
        public static string Delete = "Delete";
        public static string Login = "Login";
        public static string Logout = "Logout";
    }

    public class ModuleType
    {
        public static string User = "User";
        public static string Group = "Group";
        public static string List = "List";
        public static string Notice = "Notice";
        public static string Reply = "Reply";
    }
}
