﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using EchoChat.Channel.Factory;
using EchoChat.Constants;

namespace EchoChat.Channel
{
    public class FacebookChannel : BaseChannel
    {
        public string channelToken { get; set; }
        FacebookChannel()
        {
            channelToken = GetToken();
        }

        public string GetToken()
        {
            return string.Empty;
        }

        public override Task Process()
        {
            DelayTesting(ChannelMaster.FacebookChannel, string.Empty);
            return Task.Delay(0);
        }
    }
}
