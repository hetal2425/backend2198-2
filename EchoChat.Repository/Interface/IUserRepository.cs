﻿using EchoChat.DB;
using EchoChat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace EchoChat.Repository
{
    public interface IUserRepository
    {
        void AddUserConnection(string connectionID, Guid userID);
        string GetUserConnection(Guid userID);
        void RemoveUserConnection(string connectionID);
        int SubmitUsers(ExcelUsers list, Guid userID, int clientID);
        bool CreateUpdateUser(CreateUser user, int clientID, Guid createdBy);
        bool DeleteUser(Guid userID);
        CreateUser GetUserByUsername(string username);
        bool FirstTimeUpdatePassword(UpdatePassword update);
        bool UpdatePassword(UpdatePassword update);
        bool SetGroupAdmin(GroupAdminMapping mapping);
        List<UserSmallModel> GetUsersByListIDs(List<int> listIDs);
        UserProfile GetUserByID(Guid userID);
        UserProfile GetUserBySlackID(string slackID);
        UserClaimsModel GetUserClaimsByID(Guid userID);
        bool UpdateIntroHappened(Guid userID);
        bool UpdateUserPersonalInfo(UserPersonalInfoModel user, Guid userID);
        string GetUserPasswordByID(Guid userID);
        bool UpdateUserPassword(UserPasswordModel user, Guid userID);
        bool SaveUpdateSetting(AppSettingModel appSetting);
        bool DeleteSetting(int appSettingId);
        List<AppSettingModel> GetAppSettingByChannel(String channel);
        List<string> GetSettingKeys(bool isKarix);
        CreateUser CreateIntegrationUser(CreateUser user, int clientID, Guid createdBy);
        List<UserListDTO> GetUserIdsFromGroup(int groupId);
    }
}
