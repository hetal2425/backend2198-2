﻿using EchoChat.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EchoChat.Repository
{
    public interface IAuthRepository
    {
        UserData CheckUserLogin(LoginRequest user);
    }
}
