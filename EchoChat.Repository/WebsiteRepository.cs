﻿using AutoMapper;
using EchoChat.DB;
using EchoChat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace EchoChat.Repository
{
    public class WebsiteRepository : IWebsiteRepository
    {
        IRepositoryBase _repositoryBase = null;
        private readonly IMapper _mapper;
        public WebsiteRepository(IRepositoryBase repositoryBase, IMapper mapper)
        {
            _repositoryBase = repositoryBase;
            _mapper = mapper;
        }

        public bool SaveDemoRequest(DemoRequestDTO demo)
        {
            DemoRequest demoRequest = _mapper.Map<DemoRequestDTO, DemoRequest>(demo);
            demoRequest.DateTime = DateTime.UtcNow;
            _repositoryBase.Add<DemoRequest>(demoRequest);
            return _repositoryBase.SaveChanges();
        }
    }
}
