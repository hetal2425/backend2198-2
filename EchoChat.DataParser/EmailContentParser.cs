﻿using System;
using System.Text.RegularExpressions;

namespace EchoChat.DataParser
{
    public class EmailContentParser
    {
        public static string ParseReceivedEmailContent(string content)
        {
            try
            {
                string parsedString = content.Remove(0, content.IndexOf("quoted-printable") + 16);                

                int indexOfSecond = parsedString.IndexOf("mailto:support@echocommunicator.com");

                if (indexOfSecond > 0)
                {
                    parsedString = parsedString.Remove(indexOfSecond);
                }

                int indexOfThird = parsedString.IndexOf("quoted-printable");

                if (indexOfThird > 0)
                {
                    parsedString = parsedString.Remove(0, indexOfThird + 18);
                }

                int indexOfFourth = parsedString.IndexOf("=_");

                if (indexOfFourth > 0)
                {
                    parsedString = parsedString.Remove(indexOfFourth - 3);
                }

                parsedString = Regex.Replace(parsedString, "<.*?>", String.Empty);

                int indexOfFifth = parsedString.IndexOf("<=");

                if (indexOfFifth > 0)
                {
                    parsedString = parsedString.Remove(indexOfFifth);
                }

                int indexOfSixth = parsedString.IndexOf("<div dir=3D");

                if (indexOfSixth > 0)
                {
                    parsedString = parsedString.Remove(indexOfSixth);
                }

                int indexOfSeventh = parsedString.IndexOf("<div");

                if (indexOfSeventh > 0)
                {
                    parsedString = parsedString.Remove(indexOfSeventh);
                }

                parsedString = Regex.Replace(parsedString, "<.*?>", String.Empty);

                return parsedString;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}