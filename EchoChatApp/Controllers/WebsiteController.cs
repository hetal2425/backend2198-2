﻿using EchoChat.Channel;
using EchoChat.ChannelConnector;
using EchoChat.Constants;
using EchoChat.Helper;
using EchoChat.Models;
using EchoChat.ServiceProvider;
using EchoChatApp.General;
using log4net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EchoChatApp.Controllers
{
    [ApiController]
    [AllowAnonymous]
    [Route("api/[controller]")]
    public class WebsiteController : ControllerBase
    {
        private IWebsiteProvider websiteProvider;
        private IConnector connector;
        private ILog logger;
        public WebsiteController(IWebsiteProvider oWebsiteProvider, IConnector oConnector)
        {
            websiteProvider = oWebsiteProvider;
            connector = oConnector;
            logger = Logger.GetLogger(typeof(HookController));
        }
        [HttpPost("[action]")]
        public IActionResult SlackEndpoint() {
            return Ok();
        }

        [HttpPost("SaveDemoRequest")]
        public IActionResult SaveDemoRequest(DemoRequestDTO demo)
        {
            try
            {
                websiteProvider.SaveDemoRequest(demo);
                SendDemoRequestMail(demo.Name, demo.Org, demo.MobileNumber, demo.EmailID, demo.Date, demo.Time, demo.Remark);

                return Ok(new { Message = "SUCCESS" });
            }
            catch (Exception ex)
            {
                return BadRequest(new { Message = ex.Message });
            }

        }


        private void SendDemoRequestMail(string name, string org, string mobileNo, string emailID, string date, string time, string remark)
        {
            string Subject = "Demo Request : " + name;

            string content = $"New Demo request is received: <br/> Name: <br/>." +
                $"Name: <b>{name} </b><br/>" +
                $"Organisation: <b>{org} </b><br/>" +
                $"Mobile Number: <b>{mobileNo} </b><br/>" +
                $"Email ID: <b>{emailID} </b><br/>" +
                $"Date: <b>{date} </b><br/>" +
                $"Time: <b>{time} </b><br/>" +
                $"Remark: <b>{remark} </b><br/>";

            MessageDetails md = new MessageDetails();
            md.Title = Subject;
            md.Message = content;
            md.HTMLMessage = content;
            md.SenderDetails = AWSHelper.sender;

            List<string> channels = new List<string>();
            channels.Add(ChannelMaster.EmailChannel);

            List<UserChannelData> userChannelDatas = new List<UserChannelData>();
            userChannelDatas.Add(new UserChannelData() { EmailID = AWSHelper.sender, UserID = string.Empty, MobileNo = mobileNo });

            connector.CallBroadcaster(md, channels, userChannelDatas, new List<AppSettingsVM>());
        }
    }    
}