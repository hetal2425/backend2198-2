﻿
using EchoChat.Models;
using EchoChat.ServiceProvider;
using EchoChatApp.General;
using log4net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace EchoChatApp.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    public class ScheduleController : ControllerBase
    {
        private IUserProvider userProvider;
        private ISchedularProvider schedularProvider;
        private ILog logger;
        public ScheduleController(ISchedularProvider oschedularProvider, IUserProvider oUserProvider)
        {
            schedularProvider = oschedularProvider;
            userProvider = oUserProvider;
            logger = Logger.GetLogger(typeof(ClientController));
        }

        [Route("AddSchedule")]
        [HttpPost]
        public IActionResult AddSchedule([FromForm] SchedularDTO schedular)
        {
            try
            {
                Guid userId = Guid.Parse(this.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.PrimarySid)?.Value);
                var userDetails = userProvider.GetUserByID(userId);

                if (userDetails != null)
                {
                    schedular.UserName = userDetails.MobileNo;
                    schedular.Password = userDetails.Password;
                }
                else
                {
                    return BadRequest(new { Message = "User not found." });
                }

                if (string.IsNullOrEmpty(schedular.NoticeDetailHTML))
                {
                    return BadRequest(new { Message = "Schedule Message is Required" });
                }

                if (schedular.ScheduleTime < DateTime.Now)
                {
                    return BadRequest(new { Message = "Schedule Time is Not Lower then Current Time" });
                }

                schedularProvider.AddSchedular(schedular);
                return Ok(true);
            }
            catch (Exception ex)
            {
                logger.Error("AddSchedule" + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }

        [Route("GetSchedule")]
        [HttpGet]
        public IActionResult GetSchedule(DateTime? startDate, DateTime? endDate, int pageSize, int pageNo)
        {
            try
            {
                var schedulePagedData = schedularProvider.GetSchedulePagedData(startDate, endDate, pageSize, pageNo);
                if (schedulePagedData != null)
                {
                    return Ok(schedulePagedData);
                }
                return Ok(true);
            }
            catch (Exception ex)
            {
                logger.Error("GetSchedule" + ex.Message);
                return BadRequest(new { Message = ex.Message });
            }
        }
    }
}
