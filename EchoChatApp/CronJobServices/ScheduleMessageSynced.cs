﻿using EchoChat.Models;
using EchoChat.ServiceProvider;
using EchoChatApp.CronJobServices.CronJobExtensionMethods;
using EchoChatApp.Helper;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using RestSharp;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace EchoChatApp.CronJobServices
{
    public class ScheduleMessageSynced : CronJobService, IDisposable
    {
        private readonly IServiceScope _scope;
        private readonly AppSettingsFromFile settingsFromFile;
        public ScheduleMessageSynced(IOptions<AppSettingsFromFile> oSettingsFromFile, IScheduleConfig<ScheduleMessageSynced> config, IServiceProvider scope) : base(config.CronExpression, config.TimeZoneInfo)
        {
            settingsFromFile = oSettingsFromFile.Value;
            _scope = scope.CreateScope();
        }
        public override Task StartAsync(CancellationToken cancellationToken)
        {
            return base.StartAsync(cancellationToken);
        }
        public override Task DoWork(CancellationToken cancellationToken)
        {
            ISchedularProvider schedularProvider = _scope.ServiceProvider.GetRequiredService<ISchedularProvider>();
            try
            {
                var scheduleData = schedularProvider.GetSchedular();
                string scheduleIds = string.Empty;
                string Url = settingsFromFile.BaseAPI + "api/Notice/AddNotice";
                foreach (var item in scheduleData)
                {
                    LoginRequest loginRequest = new LoginRequest();
                    loginRequest.UserName = item.UserName;
                    loginRequest.Password = item.Password;
                    string timeZoneOffset = "330";
                    var token = schedularProvider.GenerateJwtTokenForSchedular(loginRequest, timeZoneOffset);
                    var client = new RestClient(Url);
                    client.Timeout = -1;
                    var request = new RestRequest(Method.POST);
                    request.AddHeader("Authorization", "Bearer "+ token);
                    request.AlwaysMultipartFormData = true;
                    request.AddParameter("NoticeTitle", item.NoticeTitle);
                    request.AddParameter("NoticeDetailHTML", item.NoticeDetailHTML);
                    request.AddParameter("ParentId", Convert.ToString(item.ParentId));
                    request.AddParameter("IsReply", Convert.ToString(1));
                    request.AddParameter("IsSms", Convert.ToString(Convert.ToInt32(item.IsSms)));
                    request.AddParameter("IsEmail", Convert.ToString(Convert.ToInt32(item.IsEmail)));
                    request.AddParameter("IsWhatsapp", Convert.ToString(Convert.ToInt32(item.IsWhatsapp)));
                    request.AddParameter("IsSlack", Convert.ToString(Convert.ToInt32(item.IsSlack)));
                    request.AddParameter("GroupID", Convert.ToString(item.GroupId));
                    request.AddParameter("BroadcastToAllChannel", Convert.ToString(Convert.ToInt32(item.IsBroadcastToAllChannel)));
                    request.AddParameter("StreamWithData", item.StreamWithData);
                    request.AddParameter("TemplateID", string.Empty);
                    IRestResponse response = client.Execute(request);
                    if (response.IsSuccessful)
                    {
                        if (string.IsNullOrEmpty(scheduleIds))
                            scheduleIds = Convert.ToString(item.ID);
                        else
                            scheduleIds = scheduleIds + ", " + Convert.ToString(item.ID);
                    }
                }

                if (!string.IsNullOrEmpty(scheduleIds))
                {
                    schedularProvider.RemoveSchedule(scheduleIds);
                }
            }
            catch (Exception) { }
            return Task.CompletedTask;
        }
        public override Task StopAsync(CancellationToken cancellationToken)
        {
            return base.StopAsync(cancellationToken);
        }
        public override void Dispose()
        {
            _scope?.Dispose();
        }
    }
}
